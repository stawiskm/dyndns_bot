## Quick reference
- Maintained by:
Marc Jermann

- Where to get help:
the Docker Community Slack or Stack Overflow.

## Supported tags and respective Dockerfile links
- v1.0

## Quick reference (cont.)
- Where to file issues:
https://gitlab.com/stawiskm/dyndns_bot/issues

## Supported architectures: (more info)
- linux/amd64


## What is infomaniak_DynDNS_Bot?
Using an API provided by Infomaniak, this app updates the dynamic DNS (DDNS) record for a domain name whose DNS zone is managed by Infomaniak on a regular basis. Domain names and credentials are supplied and read in using a json file. The supplemental IP address or the public IP address is then sent to the Infomaniak API along with the credentials.

![](vscode-remote://wsl%2Bubuntu/home/jerm/gitprojects/dyndns_bot/bot.jpg)

## How to use this image
### Usage
1. Create domains.json:
        
        {
        "Domains":[
        {
            "username" : "Foo",
            "password" : "Foo_pw",
            "domain" : "subdomain.example.com"
        },
        {
            "username" : "Bar",
            "password" : "Bar_pw",
            "domain" : "othersubdomain.example.com"
        }
        ]
        }
2. Create docker-compose.yml
   
        version: '3.8'
        services:
        dynDNSbot:
            image: infomaniak_dyndns_bot:v1.0
            restart: always
            volumes:
            - ./domains.json:/app/domains.json
            environment:
            HOST_IP: 0.0.0.0

3. Run: 
        
        docker-compose up

1. (OR Run:)
    
    docker pull stawiskm/infomaniak_dyndns_bot:v1.0
    
    docker run run -d -v `pwd`/domains.json:/app/domains.json stawiskm/infomaniak_dyndns_bot:v1.0

### License

MIT License

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
