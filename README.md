# DynDNS_Bot

Bot updates the dynamic DNS (DDNS) record on a regular basis.

## Description

Using an API provided by Infomaniak, this app updates the dynamic DNS (DDNS) record for a domain name whose DNS zone is managed by Infomaniak on a regular basis. Domain names and credentials are supplied and read in using a json file. The supplemental IP address or the public IP address is then sent to the Infomaniak API along with the credentials.

## Installation for Development
1. Enter directory: 
    
            cd DynDNS_Bot
2. Create and activate virtual environment:

        python3 -m venv botvenv
        source botvenv/bin/activate

3. Install requirements:

        pip install -r requirements.txt

4. You are ready ;-)

## Usage
1. Install docker
2. Install docker compose
3. Create domains.json: (see domains_EXAMPLE.json)
        
        {
        "Domains":[
        {
            "username" : "Foo",
            "password" : "Foo_pw",
            "domain" : "subdomain.example.com"
        },
        {
            "username" : "Bar",
            "password" : "Bar_pw",
            "domain" : "othersubdomain.example.com"
        }
        ]
        }
4. (optional) Adapt path of json in docker-compose.yml
5. Run: 
        
        docker-compose up

       