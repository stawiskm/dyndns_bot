# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY app.py app.py
# only app.py required

ARG HOST_IP
ENV HOST_IP=0.0.0.0
RUN echo HOST_IP

CMD ["sh", "-c", "python3 -m app.py ${HOST_IP}"]

#  docker build --tag dyndnsbot:v1.0 .
#  docker run -d -v `pwd`/domains.json:/app/domains.json -e HOST_IP=12.12.12.12 dyndnsbot