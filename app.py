import requests
import json
import logging
import time
import sys
 
logging.basicConfig(level=logging.INFO, filename='app.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s')

while True:
    try:
        # Opening JSON file
        logging.debug("Try reading json file")
        f = open('domains.json',"r")
    except Exception as e:
        logging.error("Exception occurred", exc_info=True)
        break
    # returns JSON object as 
    # a dictionary
    data = json.load(f)
    # Closing file
    f.close()
    logging.debug("Finished reading file")

    #Get IP
    # If not as arg provided 
    if not (str(sys.argv[1]) == "0.0.0.0"):
        publicIP = str(sys.argv[1])
        logging.info("Set Provided IP: %s",publicIP)
        # TODO check if IP is valid
    else:
        # If not as arg provided  get public ip
        try:
            response = requests.get("https://api.ipify.org?format=json")
            logging.info("Status Code of API to get public ip: %s",response.status_code)

        except requests.exceptions.Timeout:
            # Maybe set up for a retry, or continue in a retry loop
            logging.error("Exception occurred", exc_info=True)
            logging.warn("Public Ip address COULD NOT be catched")
            continue
        except requests.exceptions.TooManyRedirects:
            # Tell the user their URL was bad and try a different one
            logging.error("Exception occurred", exc_info=True)
            logging.warn("Public Ip address COULD NOT be catched")
            continue
        except requests.exceptions.RequestException as e:
            # catastrophic error. bail.
            logging.error("Exception occurred", exc_info=True)
            logging.critical("System Exit")
            raise SystemExit(e)
        
        if response.status_code == 200:
            publicIP = response.json()["ip"]
            logging.info("Set Public IP: %s",publicIP)
        else:
            logging.warn("Public ip: NOT FOUND")
            continue

    # Iterating through the json
    for element in data["Domains"]:
        # Read in username, pw and domain for dynDNS
        username = element["username"]
        password = element["password"]
        domain = element["domain"]

        logging.debug("Iterate over elements and red in attributes: %s, %s", username, domain)
        
        logging.info("Domain: %s",domain)

        # Post infomaniak url
        infomaniaksurl = f"https://{username}:{password}@infomaniak.com/nic/update?hostname={domain}&myip={publicIP}"
        try:
            responseFromInfomaniak = requests.post(infomaniaksurl)
            logging.info("Infomaniak Status text of API: %s",responseFromInfomaniak.text)
            logging.info("Infomaniak Status Code of API: %s",responseFromInfomaniak.status_code)

        except requests.exceptions.Timeout:
            # Maybe set up for a retry, or continue in a retry loop
            logging.error("Exception occurred", exc_info=True)
            logging.warn("Ip address COULD NOT be renewed")
            continue
        except requests.exceptions.TooManyRedirects:
            # Tell the user their URL was bad and try a different one
            logging.error("Exception occurred", exc_info=True)
            logging.warn("Ip address COULD NOT be renewed")
            continue
        except requests.exceptions.RequestException as e:
            # catastrophic error. bail.
            logging.error("Exception occurred", exc_info=True)
            logging.critical("System Exit")
            raise SystemExit(e)
        
        if responseFromInfomaniak.status_code == 200:
            logging.info("Ip address successfully renewed")
        else:
            logging.warn("Ip address COULD NOT be renewed")
            continue

    time.sleep(300)